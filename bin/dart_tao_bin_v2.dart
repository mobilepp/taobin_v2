import 'package:dart_tao_bin_v2/dart_tao_bin_v2.dart' as dart_tao_bin_v2;
import 'TaoBin.dart';

void main() {
  TaoBin taoBin = TaoBin();
  taoBin.showWelcome();
  taoBin.showCategory();
  taoBin.showMenu(taoBin.inputChoosingCategory());
  taoBin.showType(taoBin.inputChoosingMenu());
  taoBin.inputChoosingType();
  taoBin.showLevelSweet();
  taoBin.inputChoosingLevelSweet();
  taoBin.askBulb();
  taoBin.askLid();
  taoBin.showOrder();
  if (taoBin.confirmOrder().compareTo('y') == 0) {
    taoBin.showPayment();
    if (taoBin.inputChoosingPayment() == 1) {
      taoBin.checkAndChange(taoBin.inputCash());
    }
    taoBin.showStatusPayment();
    taoBin.showBenefit();
    if (taoBin.askCollectingShell().compareTo('y') == 0) {
      taoBin.askTelephone();
      taoBin.showDescriptionMember();
    }
    taoBin.showStatus();
  }
  taoBin.showThankYou();
}
