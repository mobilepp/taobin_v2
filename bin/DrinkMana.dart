import 'dart:io';
import 'Category.dart';
import 'Coffee.dart';
import 'Tea.dart';
import 'MilkCC.dart';
import 'PtShake.dart';
import 'SodaAOth.dart';

class DrinkMana {
  var catgs = <String>[];
  var ord = [];
  var choosing = [];

  Category Coffee = Category('Coffee');
  Category Tea = Category('Tea');
  Category MilkCC = Category('Milk Cocoa and Caramel');
  Category PtShake = Category('Protein Shake');
  Category SodaAOth = Category('Soda and Others');
  Category Reccommend = Category('Recommended Menu');

  void addCategory() {
    catgs.add(Coffee.getName);
    catgs.add(Tea.getName);
    catgs.add(MilkCC.getName);
    catgs.add(PtShake.getName);
    catgs.add(SodaAOth.getName);
    catgs.add(Reccommend.getName);
  }

  void showCategory() {
    print('All Category of TAO BIN : ');
    for (var i = 0; i < catgs.length; i++) {
      int n = i + 1;
      stdout.write('$n : ');
      print(catgs[i]);
    }
    print('----------');
  }

  void getCategory(int choosingCatg) {
    for (var i = 0; i < catgs.length; i++) {
      if (i == choosingCatg - 1) {
        // choosing.add({'Category Menu', catgs[i]});
        ord.add('Category Menu : ');
        choosing.add(catgs[i]);
        break;
      }
    }
  }

  var sweetMap = {
    1: 'No Sugar',
    2: 'Little Sweet',
    3: 'Medium',
    4: 'So Sweet',
    5: 'Very Sweet; Sweet 3 Worlds'
  };

  void showSweetMap() {
    print('Level\'s Sweet of TAO BIN :');
    sweetMap.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }

  int mn = -1;
  int showMenu(int choosingCatg) {
    switch (choosingCatg) {
      case 1:
        Coffee.showMenuInCategory();
        break;
      case 2:
        Tea.showMenuInCategory();
        break;
      case 3:
        MilkCC.showMenuInCategory();
        break;
      case 4:
        PtShake.showMenuInCategory();
        break;
      case 5:
        SodaAOth.showMenuInCategory();
        break;
      case 6:
        Reccommend.showMenuInCategory();
        break;
    }
    mn = choosingCatg;
    return mn;
  }

  void showTypeMenu(int choosingMenu) {
    switch (mn) {
      case 1:
        Coffee.showMenuAndType(choosingMenu);
        break;
      case 2:
        Tea.showMenuAndType(choosingMenu);
        break;
      case 3:
        MilkCC.showMenuAndType(choosingMenu);
        break;
      case 4:
        PtShake.showMenuAndType(choosingMenu);
        break;
      case 5:
        SodaAOth.showMenuAndType(choosingMenu);
        break;
      case 6:
        Reccommend.showMenuAndType(choosingMenu);
        break;
    }
  }

  void getMenu(int choosingMenu) {
    switch (mn) {
      case 1:
        ord.add('Menu : ');
        choosing.add(Coffee.getMenuNameUnique(choosingMenu));
        break;
      case 2:
        ord.add('Menu : ');
        choosing.add(Tea.getMenuNameUnique(choosingMenu));
        break;
      case 3:
        ord.add('Menu : ');
        choosing.add(MilkCC.getMenuNameUnique(choosingMenu));
        break;
      case 4:
        ord.add('Menu : ');
        choosing.add(PtShake.getMenuNameUnique(choosingMenu));
        break;
      case 5:
        ord.add('Menu : ');
        choosing.add(SodaAOth.getMenuNameUnique(choosingMenu));
        break;
      case 6:
        ord.add('Menu : ');
        choosing.add(Reccommend.getMenuNameUnique(choosingMenu));
        break;
    }
  }

  void getTypePrice(int choosingType) {
    switch (mn) {
      case 1:
        var token = Coffee.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
      case 2:
        var token = Tea.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
      case 3:
        var token = MilkCC.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
      case 4:
        var token = PtShake.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
      case 5:
        var token = SodaAOth.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
      case 6:
        var token = Reccommend.getTypePrice(choosingType).split(' ');
        TPMana(token);
        break;
    }
  }

  void TPMana(List<String> token) {
    ord.add('Type of Menu : ');
    choosing.add(token[token.length - 2]);
    ord.add('Price : ');
    choosing.add(token[token.length - 1]);
  }

  void addMenuItem() {
    Reccommend.addMenu(CoffeeMenu('Espresso', 'Hot', 25));
    Reccommend.addMenu(CoffeeMenu('Americano', 'Hot ', 30));
    Reccommend.addMenu(CoffeeMenu('Americano', 'Iced ', 35));
    Reccommend.addMenu(CoffeeMenu('Cafe Latte', 'Hot', 35));
    Reccommend.addMenu(CoffeeMenu('Cafe Latte', 'Iced', 40));
    Reccommend.addMenu(CoffeeMenu('Cafe Latte', 'Smoothie', 45));
    Reccommend.addMenu(TeaMenu('Thai Milk Tea', 'Hot', 35));
    Reccommend.addMenu(TeaMenu('Thai Milk Tea', 'Iced', 40));
    Reccommend.addMenu(TeaMenu('Thai Milk Tea', 'Smoothie', 45));
    Reccommend.addMenu(MilkCCMenu('Cocoa', 'Hot', 30));
    Reccommend.addMenu(MilkCCMenu('Cocoa', 'Iced', 35));
    Reccommend.addMenu(MilkCCMenu('Cocoa', 'Smoothie', 40));
    Reccommend.addMenu(MilkCCMenu('Caramel Milk', 'Hot', 35));
    Reccommend.addMenu(MilkCCMenu('Caramel Milk', 'Iced', 40));
    Reccommend.addMenu(MilkCCMenu('Caramel Milk', 'Smoothie', 45));
    Reccommend.addMenu(MilkCCMenu('Pink Milk', 'Iced', 35));
    Reccommend.addMenu(MilkCCMenu('Pink Milk', 'Smoothie', 40));
    Reccommend.addMenu(MilkCCMenu('Smoothie Strawberry Milk', 'Smoothie', 45));
    Reccommend.addMenu(MilkCCMenu('Smoothie Volcano Oreo', 'Smoothie', 55));
    Reccommend.addMenu(SodaAOthMenu('Pepsi', 'Iced', 15));
    Reccommend.addMenu(SodaAOthMenu('Pepsi', 'Smoothie', 20));
    Reccommend.addMenu(SodaAOthMenu('Plum Pepsi', 'Iced', 20));
    Reccommend.addMenu(SodaAOthMenu('Plum Pepsi', 'Smoothie', 25));
    Reccommend.addMenu(SodaAOthMenu('Powerful Turtle', 'Iced', 15));
    Reccommend.addMenu(SodaAOthMenu('Lime Plum Soda', 'Iced', 25));
    Reccommend.addMenu(SodaAOthMenu('Lime Plum Soda', 'Smoothie', 30));

    Coffee.addMenu(CoffeeMenu('Espresso', 'Hot', 25));
    Coffee.addMenu(CoffeeMenu('Double Espresso', 'Hot', 35));
    Coffee.addMenu(CoffeeMenu('Americano', 'Hot', 30));
    Coffee.addMenu(CoffeeMenu('Americano', 'Iced', 35));
    Coffee.addMenu(CoffeeMenu('Espresso Shot Blue Daddy', 'Hot', 55));
    Coffee.addMenu(CoffeeMenu('Americano Soda', 'Iced', 40));
    Coffee.addMenu(CoffeeMenu('Americano Blue Daddy', 'Hot', 60));
    Coffee.addMenu(CoffeeMenu('Americano Blue Daddy', 'Iced', 65));
    Coffee.addMenu(CoffeeMenu('Blue Daddy Almost Dirty', 'Iced', 70));
    Coffee.addMenu(CoffeeMenu('Iced Espresso', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Iced Espresso', 'Smoothie', 50));
    Coffee.addMenu(CoffeeMenu('Cafe Latte', 'Hot', 35));
    Coffee.addMenu(CoffeeMenu('Cafe Latte', 'Iced', 40));
    Coffee.addMenu(CoffeeMenu('Cafe Latte', 'Smoothie', 45));
    Coffee.addMenu(CoffeeMenu('Cappuccino', 'Hot', 35));
    Coffee.addMenu(CoffeeMenu('Cappuccino', 'Iced', 40));
    Coffee.addMenu(CoffeeMenu('Cappuccino', 'Smoothie', 45));
    Coffee.addMenu(CoffeeMenu('Mocha', 'Hot', 40));
    Coffee.addMenu(CoffeeMenu('Mocha', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Mocha', 'Smoothie', 50));
    Coffee.addMenu(CoffeeMenu('Caramel Latte', 'Hot', 40));
    Coffee.addMenu(CoffeeMenu('Caramel Latte', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Caramel Latte', 'Smoothie', 50));
    Coffee.addMenu(CoffeeMenu('Matcha Latte', 'Hot', 45));
    Coffee.addMenu(CoffeeMenu('Matcha Latte', 'Iced', 50));
    Coffee.addMenu(CoffeeMenu('Matcha Latte', 'Smoothie', 55));
    Coffee.addMenu(CoffeeMenu('Brown Sugar Latte', 'Hot', 40));
    Coffee.addMenu(CoffeeMenu('Brown Sugar Latte', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Brown Sugar Latte', 'Smoothie', 50));
    Coffee.addMenu(CoffeeMenu('Thai Tea Cafe Latte', 'Hot', 40));
    Coffee.addMenu(CoffeeMenu('Thai Tea Cafe Latte', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Taiwanese Tea Cafe Latte', 'Hot', 40));
    Coffee.addMenu(CoffeeMenu('Taiwanese Tea Cafe Latte', 'Iced', 45));
    Coffee.addMenu(CoffeeMenu('Taiwanese Tea Cafe Latte', 'Smoothie', 50));
    Coffee.addMenu(CoffeeMenu('Lychee Americano', 'Hot ', 30));
    Coffee.addMenu(CoffeeMenu('Lychee Americano', 'Iced ', 35));

    Tea.addMenu(TeaMenu('Chrysanthemum Tea', 'Hot', 20));
    Tea.addMenu(TeaMenu('Chrysanthemum Tea', 'Iced', 25));
    Tea.addMenu(TeaMenu('Chrysanthemum Tea', 'Smoothie', 30));
    Tea.addMenu(TeaMenu('Ginger Tea', 'Hot', 20));
    Tea.addMenu(TeaMenu('Ginger Tea', 'Iced', 25));
    Tea.addMenu(TeaMenu('Ginger Tea', 'Smoothie', 30));
    Tea.addMenu(TeaMenu('Lime Ginger Tea', 'Hot', 20));
    Tea.addMenu(TeaMenu('Thai Milk Tea', 'Hot', 35));
    Tea.addMenu(TeaMenu('Thai Milk Tea', 'Iced', 40));
    Tea.addMenu(TeaMenu('Thai Milk Tea', 'Smoothie', 45));
    Tea.addMenu(TeaMenu('Taiwanese Tea', 'Hot', 40));
    Tea.addMenu(TeaMenu('Taiwanese Tea', 'Iced', 45));
    Tea.addMenu(TeaMenu('Taiwanese Tea', 'Smoothie', 50));
    Tea.addMenu(TeaMenu('Matcha Latte', 'Hot', 40));
    Tea.addMenu(TeaMenu('Matcha Latte', 'Iced', 45));
    Tea.addMenu(TeaMenu('Matcha Latte', 'Smoothie', 50));
    Tea.addMenu(TeaMenu('Japanese Green Tea', 'Hot', 20));
    Tea.addMenu(TeaMenu('Japanese Green Tea', 'Iced', 30));
    Tea.addMenu(TeaMenu('Japanese Green Tea', 'Smoothie', 35));
    Tea.addMenu(TeaMenu('Lime Japanese Green Tea', 'Hot', 25));
    Tea.addMenu(TeaMenu('Lime Japanese Green Tea', 'Iced', 35));
    Tea.addMenu(TeaMenu('Lime Japanese Green Tea', 'Smoothie', 40));
    Tea.addMenu(TeaMenu('Black Tea', 'Hot', 25));
    Tea.addMenu(TeaMenu('Black Tea', 'Iced', 30));
    Tea.addMenu(TeaMenu('Lime Black Tea', 'Hot', 25));
    Tea.addMenu(TeaMenu('Lime Black Tea', 'Iced', 30));
    Tea.addMenu(TeaMenu('Tiwanese Black Tea', 'Hot', 20));
    Tea.addMenu(TeaMenu('Tiwanese Black Tea', 'Iced', 25));
    Tea.addMenu(TeaMenu('Lime Tiwanese Black Tea', 'Hot', 25));
    Tea.addMenu(TeaMenu('Lime Tiwanese Black Tea', 'Iced', 30));

    MilkCC.addMenu(MilkCCMenu('Milk', 'Hot', 30));
    MilkCC.addMenu(MilkCCMenu('Milk', 'Iced', 35));
    MilkCC.addMenu(MilkCCMenu('Milk', 'Smoothie', 40));
    MilkCC.addMenu(MilkCCMenu('Caramel Milk', 'Hot', 35));
    MilkCC.addMenu(MilkCCMenu('Caramel Milk', 'Iced', 40));
    MilkCC.addMenu(MilkCCMenu('Caramel Milk', 'Smoothie', 45));
    MilkCC.addMenu(MilkCCMenu('Caramel Cocoa', 'Hot', 35));
    MilkCC.addMenu(MilkCCMenu('Caramel Cocoa', 'Iced', 40));
    MilkCC.addMenu(MilkCCMenu('Caramel Cocoa', 'Smoothie', 45));
    MilkCC.addMenu(MilkCCMenu('Brown Sugar Milk', 'Hot', 35));
    MilkCC.addMenu(MilkCCMenu('Brown Sugar Milk', 'Iced', 40));
    MilkCC.addMenu(MilkCCMenu('Brown Sugar Milk', 'Smoothie', 45));
    MilkCC.addMenu(MilkCCMenu('Pink Milk', 'Iced', 35));
    MilkCC.addMenu(MilkCCMenu('Pink Milk', 'Smoothie', 40));
    MilkCC.addMenu(MilkCCMenu('Cocoa', 'Hot', 30));
    MilkCC.addMenu(MilkCCMenu('Cocoa', 'Iced', 35));
    MilkCC.addMenu(MilkCCMenu('Cocoa', 'Smoothie', 40));
    MilkCC.addMenu(MilkCCMenu('Smoothie Strawberry Cocoa', 'Smoothie', 45));
    MilkCC.addMenu(MilkCCMenu('Smoothie Strawberry Milk', 'Smoothie', 45));
    MilkCC.addMenu(MilkCCMenu('Smoothie Volcano Oreo', 'Smoothie', 55));

    PtShake.addMenu(PtShakeMenu('Macha Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Cocoa Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Strawberry Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Espresso Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Thai Milk Tea Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Brown Sugar Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Taiwanese Tea Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Caramel Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Milk Protein Shake', 'Iced', 65));
    PtShake.addMenu(PtShakeMenu('Pure Protein Shake', 'Iced', 60));

    SodaAOth.addMenu(SodaAOthMenu('Pepsi', 'Iced', 15));
    SodaAOth.addMenu(SodaAOthMenu('Pepsi', 'Smoothie', 20));
    SodaAOth.addMenu(SodaAOthMenu('Powerful Turtle', 'Iced', 15));
    SodaAOth.addMenu(SodaAOthMenu('Limenade Soda', 'Iced', 20));
    SodaAOth.addMenu(SodaAOthMenu('Limenade Soda', 'Smoothie', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lychee Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lychee Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Strawberry Soda', 'Iced', 35));
    SodaAOth.addMenu(SodaAOthMenu('Strawberry Soda', 'Smoothie', 40));
    SodaAOth.addMenu(SodaAOthMenu('Sala Soda', 'Iced', 20));
    SodaAOth.addMenu(SodaAOthMenu('Sala Soda', 'Smoothie', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lime Sala Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lime Sala Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Plum Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Plum Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Lime Plum Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lime Plum Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Plum Pepsi', 'Iced', 20));
    SodaAOth.addMenu(SodaAOthMenu('Plum Pepsi', 'Smoothie', 25));
    SodaAOth.addMenu(SodaAOthMenu('Ginger Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Ginger Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Lime Ginger Soda', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lime Ginger Soda', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Soda', 'Iced', 10));
    SodaAOth.addMenu(SodaAOthMenu('Iced Soda', 'Iced', 10));
    SodaAOth.addMenu(SodaAOthMenu('Sala', 'Iced', 20));
    SodaAOth.addMenu(SodaAOthMenu('Sala', 'Smoothie', 25));
    SodaAOth.addMenu(SodaAOthMenu('Limenade', 'Hot', 20));
    SodaAOth.addMenu(SodaAOthMenu('Limenade', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Limenade', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Plum', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Plum', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Lychee', 'Iced', 25));
    SodaAOth.addMenu(SodaAOthMenu('Lychee', 'Smoothie', 30));
    SodaAOth.addMenu(SodaAOthMenu('Strawberry', 'Smoothie', 35));
    SodaAOth.addMenu(SodaAOthMenu('Iced', 'Iced', 10));
    SodaAOth.addMenu(SodaAOthMenu('Water', 'Hot', 10));
    SodaAOth.addMenu(SodaAOthMenu('Water', 'Iced', 10));
  }
}
