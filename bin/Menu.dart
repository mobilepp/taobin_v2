abstract class Menu {
  String name = '';
  String type = '';
  int price = 0;

  Menu(this.name, this.type, this.price);

  String get getName {
    return name;
  }

  String get getType {
    return type;
  }

  int get getPrice {
    return price;
  }

  @override
  String toString() {
    return '$name $type ฿$price';
  }
}
