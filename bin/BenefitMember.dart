import 'dart_app_calculate.dart';

class BenefitMember {
  String levelShell = '';
  String description = '';

  BenefitMember(this.levelShell, this.description);

  String get getlevelShell {
    return levelShell;
  }

  String get getdescription {
    return description;
  }

  @override
  String toString() {
    return 'levelShell : $levelShell : $description';
  }

  String levelShellBenefit(String status, int point, String price0) {
    String price1 = price0.substring(1);
    double price = double.parse(price1);
    if (point >= 350) {
      print(
          'You get exclusive coupon for exchange your favorite menu and special gift!');
      return 'Level : Oldster Turtle';
    } else if (point >= 150) {
      double cc = calcPointEvery15(price);
      print(
          'You get $cc Winged Turtle armatures and exclusive coupon for exchange your favorite menu!');
      present(point, cc);
      return 'Level : Winged Turtle';
    } else if (status.compareTo('-') == 0) {
      double cc = calcPointEvery20(price);
      print('You get $cc Shyny Turtle armatures!');
      present(point, cc);
      return 'Level : Shyny Turtle';
    } else if (status.compareTo('Member LINE@TAOBIN') == 0) {
      double cc = calcPointEvery15(price);
      print('You get $cc Totter Turtle armatures and dicount ฿50!');
      present(point, cc);
      return 'Level : Totter Turtle';
    }
    return '';
  }

  void present(int point, double cc) {
    Calculate p = Calculate((point * 1.0), '+', cc);
    double pp = p.calculate().floorToDouble();
    print('Your Turtle Shell now : $pp');
  }

  double calcPointEvery20(double price) {
    Calculate c = Calculate(price, '/', 20);
    double cc = c.calculate().floorToDouble();
    return cc;
  }

  double calcPointEvery15(double price) {
    Calculate c = Calculate(price, '/', 15);
    double cc = c.calculate().floorToDouble();
    return cc;
  }
}
