import 'Menu.dart';
import 'dart_app_calculate.dart';

class Payment implements Menu {
  @override
  String name = '';

  @override
  int price = 0;

  @override
  String type = '';

  Payment(this.type);

  @override
  String get getName => throw UnimplementedError();

  @override
  int get getPrice => throw UnimplementedError();

  @override
  String get getType {
    return type;
  }

  double isCash(double pay, String price0) {
    String price1 = price0.substring(1);
    double price = double.parse(price1);
    Calculate calc = Calculate(pay, '-', price);
    return calc.calculate();
  }
}
