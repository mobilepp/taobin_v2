import 'dart:io';
import 'Menu.dart';

class Category {
  String name = '';
  var menus = <Menu>[];
  var nameList = <String>[];
  var nameList1 = ['m'];
  String namem = '';
  var data = <Menu>[];

  Category(this.name);

  String get getName {
    return name;
  }

  void addMenu(Menu menu) {
    menus.add(menu);
  }

  List showMenuInCategory() {
    print('All Menu in Category : ');
    int n = 1;
    nameList.add(menus[0].getName);
    stdout.write('$n : ');
    print(menus[0].getName);
    for (var i = 0; i < menus.length; i++) {
      if (i > 0 && menus[i].getName.compareTo(menus[i - 1].getName) != 0) {
        n += 1;
        nameList.add(menus[i].getName);
        stdout.write('$n : ');
        print(menus[i].getName);
      }
    }
    print('------------');
    nameList1 = nameList;
    return nameList1;
  }

  List showMenuAndType(int choosingMenu) {
    print('All Type of Menu : ');
    namem = nameOfChoosingMenu(choosingMenu, namem, nameList);
    int n = 1;
    for (var i = 0; i < menus.length; i++) {
      if (namem.compareTo(menus[i].getName) == 0) {
        stdout.write('$n : ');
        print(menus[i].toString());
        data.add(menus[i]);
        n += 1;
      }
    }
    print('------------');
    return data;
  }

  String getMenuNameUnique(int choosingMenu) {
    String namem = '';
    namem = nameOfChoosingMenu(choosingMenu, namem, nameList1);
    return namem;
  }

  String nameOfChoosingMenu(int choosingMenu, String namem, List nameList) {
    for (var i = 0; i < nameList.length; i++) {
      if (i == (choosingMenu - 1)) {
        namem = nameList[i];
        break;
      }
    }
    return namem;
  }

  String getTypePrice(int choosingType) {
    String menu = '';
    for (var i = 0; i < data.length; i++) {
      if (i == choosingType - 1) {
        menu = data[i].toString();
      }
    }
    return menu;
  }
}
