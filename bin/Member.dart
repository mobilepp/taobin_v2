class Member {
  String name = '';
  String telephoneNum = '';
  String status = '';
  int qtyTurtleShell = 0;

  Member(this.name, this.telephoneNum, this.status, this.qtyTurtleShell);

  String get getName {
    return name;
  }

  String get gettelephoneNum {
    return telephoneNum;
  }

  String get getStatus {
    return status;
  }

  int get getqtyTurtleShell {
    return qtyTurtleShell;
  }

  @override
  String toString() {
    return 'name : $name tel. : $telephoneNum status : $status TurtleShell : $qtyTurtleShell';
  }
}
