import 'DrinkMana.dart';
import 'Payment.dart';
import 'Member.dart';
import 'BenefitMember.dart';
import 'dart:io';

class TaoBin {
  int choosingCatg = -1;
  int choosingMenu = -1;
  int choosingType = -1;
  int choosingLevelSweet = -1;
  String bulbYN = '';
  String lidYN = '';
  String orderYN = '';
  int choosingPayment = -1;
  double pay = -1;
  double change = -1;
  String coll = '';
  String tel = '';
  var ord = [];
  var choosing = [];

  DrinkMana drinkMana = DrinkMana();

  void showWelcome() {
    print('Welcome to happyness with TAO BIN!');
    print('----------');
  }

  void showCategory() {
    drinkMana.addCategory();
    drinkMana.showCategory();
  }

  int inputChoosingCategory() {
    print('Please choosing your interest category. : ');
    choosingCatg = int.parse(stdin.readLineSync()!);
    drinkMana.getCategory(choosingCatg);
    return choosingCatg;
  }

  void showMenu(int choosingCatg) {
    drinkMana.addMenuItem();
    drinkMana.showMenu(choosingCatg);
  }

  int inputChoosingMenu() {
    print('Please choosing your interest menu. : ');
    choosingMenu = int.parse(stdin.readLineSync()!);
    drinkMana.getMenu(choosingMenu);
    return choosingMenu;
  }

  void showType(int choosingMenu) {
    drinkMana.showTypeMenu(choosingMenu);
  }

  void inputChoosingType() {
    choosingType = int.parse(stdin.readLineSync()!);
    drinkMana.getTypePrice(choosingType);
  }

  void showLevelSweet() {
    drinkMana.showSweetMap();
  }

  void inputChoosingLevelSweet() {
    choosingLevelSweet = int.parse(stdin.readLineSync()!);
    ord.add('Level\'s Sweet : ');
    choosing.add(drinkMana.sweetMap[choosingLevelSweet]);
  }

  void askBulb() {
    print('Have you want a bulb from TAO BIN (y/n) : ');
    bulbYN = stdin.readLineSync()!;
    ord.add('Bulb : ');
    choosing.add(bulbYN);
  }

  void askLid() {
    print('Have you want a lid from TAO BIN (y/n) : ');
    lidYN = stdin.readLineSync()!;
    ord.add('Lid : ');
    choosing.add(lidYN);
  }

  void showOrder() {
    print('----------');
    print('You Order : ');
    // print(drinkMana.choosing);
    for (var i = 0; i < drinkMana.choosing.length; i++) {
      stdout.write(drinkMana.ord[i]);
      print(drinkMana.choosing[i]);
    }
    // print(choosing);
    for (var i = 0; i < choosing.length; i++) {
      stdout.write(ord[i]);
      print(choosing[i]);
    }
  }

  String confirmOrder() {
    print('You want to confirm order? (y/n) : ');
    orderYN = stdin.readLineSync()!;
    return orderYN;
  }

  Payment Cash = Payment('Cash');
  Payment QRCode = Payment('Scan QR');
  Payment TBCredit = Payment('TAO BIN Credit');
  Payment coupon = Payment('Use/View Coupon');
  Payment ewallet = Payment('E-Wallet');
  Payment othProm = Payment('Other Promotions');

  var payment = [];

  void addPayment() {
    payment.add(Cash.getType);
    payment.add(QRCode.getType);
    payment.add(TBCredit.getType);
    payment.add(coupon.getType);
    payment.add(ewallet.getType);
    payment.add(othProm.getType);
  }

  void showPayment() {
    addPayment();
    print('All Payment of TAO BIN : ');
    for (var i = 0; i < payment.length; i++) {
      int n = i + 1;
      stdout.write('$n : ');
      print(payment[i]);
    }
    print('----------');
  }

  int c = -1;
  int inputChoosingPayment() {
    print('Which you want to pay on TAO BIN :');
    choosingPayment = int.parse(stdin.readLineSync()!);
    c = choosingPayment;
    return c;
  }

  double inputCash() {
    print('Please input your money in box of TAO BIN: ');
    pay = double.parse(stdin.readLineSync()!);
    return pay;
  }

  void checkAndChange(double pay) {
    while (Cash.isCash(pay, drinkMana.choosing[3]) < 0) {
      double cc = Cash.isCash(pay, drinkMana.choosing[3]);
      pay = getMoreMoneyUntilEnough(cc, pay);
    }
    change = Cash.isCash(pay, drinkMana.choosing[3]);
    if (change != 0) print('Please get yor change ฿$change');
  }

  double getMoreMoneyUntilEnough(double cc, double pay) {
    double ccp = -1 * cc;
    print('Please input your money more than or equal $ccp');
    double addP = double.parse(stdin.readLineSync()!);
    pay += addP;
    return pay;
  }

  void showStatusPayment() {
    if (c > 1) {
      print('Please do following their step');
    }
    print('----------');
    print('= Success Payment =');
  }

  Member mb = Member('mukku', '0625319822', 'Member LINE@TAOBIN', 58);
  Member mb2 = Member('wyb', '0663230805', '-', 85);

  var member = <Member>[];

  void addMember() {
    member.add(mb);
    member.add(mb2);
  }

  String askCollectingShell() {
    print('You want to collecting turtle\'s shell ? (y/n) : ');
    coll = stdin.readLineSync()!;
    return coll;
  }

  String askTelephone() {
    print('Enter your telephone number: ');
    tel = stdin.readLineSync()!;
    return tel;
  }

  BenefitMember b = BenefitMember('', '');
  BenefitMember b1 = BenefitMember('Shyny Turtle',
      'when collecting turtle shells with our mobile number, \nyou will receive 1 armature for every 20 baht purchase.');
  BenefitMember b2 = BenefitMember('Totter Turtle',
      'When registering as a flying turtle member at LINE@TAOBIN, you will receive \n1 armature with every purchase of 15 baht and get the right to redeem a drink \nwith a discount of 50 in the month of birth 1 time.');
  BenefitMember b3 = BenefitMember('Winged Turtle',
      'When a total of 150 turtle shells accumulated, you will receive \n1 armature. For every purchase of 15 baht, get a free drink coupon. \nWhen the status is postponed as scheduled and get a free redemption right \nin the month of birth 1 time');
  BenefitMember b4 = BenefitMember('Oldster Turtle',
      'When you have accumulated 350 turtle shells, you will receive \na free drink coupon. When the status is postponed as scheduled And get \na free redemption in the month of birth 1 time. Specially, \nreceive a New Year gift from TAO BIN.');

  var benefit = [];

  void addBenefit() {
    benefit.add(b1);
    benefit.add(b2);
    benefit.add(b3);
    benefit.add(b4);
  }

  void showBenefit() {
    addBenefit();
    print('----------');
    print('All Member Benefit of TAO BIN : ');
    for (var i = 0; i < benefit.length; i++) {
      int n = i + 1;
      stdout.write('$n : ');
      print(benefit[i]);
    }
    print('----------');
  }

  void showDescriptionMember() {
    addMember();
    for (var i = 0; i < member.length; i++) {
      if (tel.compareTo(member[i].gettelephoneNum) == 0) {
        print('----------');
        stdout.write('Name : ');
        print(member[i].getName);
        stdout.write('Telephone : 06x-xxx-');
        print(member[i].gettelephoneNum.substring(6));
        stdout.write('Status : ');
        print(member[i].getStatus);
        stdout.write('Your turtle\'s shell : ');
        print(member[i].getqtyTurtleShell);
        print(b.levelShellBenefit(member[i].getStatus,
            member[i].getqtyTurtleShell, drinkMana.choosing[3]));
      }
    }
  }

  void showStatus() {
    print('----------');
    print('Please take your bulb and lid between waiting me');
    print('----------');
    print('----------');
    print('SuccessFul! Please take your menu');
    print('----------');
  }

  void showThankYou() {
    print('----------');
    print('Thank you, Have a nice day!');
  }
}
